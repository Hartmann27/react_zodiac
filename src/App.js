import React from "react";
import './index.css'

function App() {
    const ZnackiZodiack = [
        {
            id: 1,
            name: 'Козоріг:',
            src: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSUSVT18FjXY5Qswnojx1xABtiogXUcaTwYmA&usqp=CAU',
            text: "Козорі́г (лат. Capricorn) — десятий знак зодіаку,Згідно з координатами західної астрології Сонце знаходиться в знаку Козорога з 22 грудня по 20 січня, ведичної — Макара з 14 січня до 13 лютого,кардинальний знак тригона — Земля."
        },
        {
            id: 2,
            name: 'Овен:',
            src: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcS1aeyKxj0NjZWCLjxgU1iANTNT6ARg7wgapg&usqp=CAU',
            text: 'Овéн (лат. Aries - баран) — перший знак зодіаку,У західній астрології вважається, що Сонце знаходиться в знаку Овна приблизно з 21 березня по 20 квітня,кардинальний знак тригону Вогонь.'
        },
        {
            id: 3,
            name: 'Телець:',
            src: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRa_lXNJcVnA_n0vX_1Ij6pBix8IXB6bP1ilg&usqp=CAU',
            text: "Теле́ць (лат. Taurus) — другий знак зодіаку,Відповідно до західної астрологічної традиції Сонце перебуває в знаку Тельця приблизно з 21 квітня по 21 травня,постійний знак тригону Землі."
        },
        {
            id: 4,
            name: 'Скорпіон:',
            src: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRdsihaxB82jEhVvLm7veM8zhLn8mX2W-5IfA&usqp=CAU",
            text: 'Скорпіон (лат. Scorpio) — восьмий знак зодіаку,постійний знак тригона Вода,Згідно із західною астрологією, Сонце перебуває у знаку Скорпіона приблизно з 24 жовтня по 22 листопада.'
        },
        {
            id: 5,
            name: 'Стрілець:',
            src: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ1wOywDP1zTvZmpV7kAi_T8aX6jgNldm5_Ww&usqp=CAU',
            text: 'Стрілець (лат. Sagittarius) — девятий знак зодіаку,знак тригона — Вогонь,Згідно з координатами західної астрології Сонце знаходиться в знаку Стрільця приблизно з 23 листопада по 21 грудня.'
        },
        {
            id: 6,
            name: 'Водолій:',
            src: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRpJMBx4HYhzTBOxgzSTUqZ4Xkj_I2XjSeeYg&usqp=CAU',
            text: 'Водолій (лат. Aquarius) — одинадцятий знак зодіаку,фіксований знак тригону — Повітря,У західній астрології вважають, що Сонце перебуває в знаку Водолія з приблизно 21 січня по 20 лютого.'
        },
        {
            id: 7,
            name: 'Близнята:',
            src: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSz72qlePJ-mHdho7gpyb5XNkAoQn7G3noLBA&usqp=CAU',
            text: "Близня́та (лат. Gemini) — одне з 12 сузір'їв зодіаку,в Україні Близнят найкраще видно пізньої осені, взимку та ранньої весни. Сонце перебуває у сузір'ї з 21 червня до 20 липня."
        },
        {
            id: 8,
            name: 'Рак:',
            src: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcS3cBmg-T13IsFcfYFO7HvrSqqEcK4kYilNFA&usqp=CAU',
            text: "Рак (лат. Cancer) — четвертий знак зодіаку,кардинальний знак тригона Вода,В західній астрології вважається, що Сонце знаходиться в знаку Рака приблизно з 22 червня по 22 липня."
        },
        {
            id: 9,
            name: 'Лев:',
            src: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQwYrU41ZH2h21TiLXS6mHC3qPAMt16zAhZe20vAhavJrZY47___rvGTs_oKJQMzyIzbnc&usqp=CAU',
            text: "Лев (лат. Leo) — п'ятий знак зодіаку,постійний знак тригона Вогонь,В західній астрології вважається, що Сонце знаходиться в знаку Лева приблизно з 23 липня по 23 серпня."
        },
        {
            id: 10,
            name: 'Діва:',
            src: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcS7RdlpB4hIpnWdEvOw1p1Phu5oWbd5oSMDQCUd9IJGaPL4e2RUmPwfA12uRI2oAntZ1K0&usqp=CAU',
            text: "Діва (лат. Virgo) — шостий знак зодіаку,мутабельний знак тригона Земля,У західній астрології вважається, що Сонце знаходиться в знаку Діви приблизно з 24 серпня по 22 вересня."
        },
        {
            id: 11,
            name: 'Терези:',
            src: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSqIEfjGAhcmhhcIc6dEOfF4Ub2Li-FGE__9P_KKY1eZ4BXBfEu5Je4215IYZ3itwABvVM&usqp=CAU',
            text: "Терези́ (лат. Libra)  — сьомий знак зодіаку,Терези є чоловічим кардинальним повітряним знаком,Згідно із західною астрологічною традицією вважається, що Сонце знаходиться в знаку Терезів приблизно з 23 вересня по 23 жовтня."
        },
        {
            id: 12,
            name: 'Риби:',
            src: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTXVGKKaErD6v7kmnzM3I1xwmLw3SE9rnPW4e_hPvFAzC1ngk_y8gWxcmTpO0czpdDnhU4&usqp=CAU',
            text: "Риби (лат. Pisces) — дванадцятий знак зодіаку,жіночий мутабельний знак тригона Води, зимовий,Згідно з координатами зодіаку західної традиції Сонце перебуває в знаку Риб приблизно з 21 лютого по 20 березня."
        },
        {
            id: 13,
            name: 'Змієносець:',
            src: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ7ko3K6gMBHZIKCmJYg4-Gztnv-nl2v7Y2L2xfT_UtNJu_8nEcZ16al-m9JveljTiftQw&usqp=CAU',
            text: "Змієно́сець (лат. Ophiuchus) — знак, який західна та ведична астрологічні традиції традиційно не включають до зодіакальних знаків,Виявляється не завжди. Умова прояви — наявність планет у секторах обох знаків зодіаку.Під вплив Змієносця потрапляють люди, що народилися в період з 30 листопада до 18 грудня."
        },
    ]

    const block = ZnackiZodiack.map((el) => {
        return (
            <div key={el.id} className="style-div">
                <p className="style-p">{el.name}</p>
                <p className="style-text">{el.text}</p>
                <img src={el.src} />
            </div>
        )
    })
    return (
        <>
            <p className="aling">Знаки Зодіаку</p>
            <div className="style-block">
                {block}
            </div>
        </>
    )
}
export default App;
